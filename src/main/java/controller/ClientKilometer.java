package controller;

import mtn.wsdl.ChangeLengthUnit;
import mtn.wsdl.ChangeLengthUnitResponse;
import mtn.wsdl.Lengths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The client contains one method: getQuote which does the actual SOAP exchange.
 * @author kevinjanvier
 */

public class ClientKilometer extends WebServiceGatewaySupport {
	private static final Logger log =
			LoggerFactory.getLogger(ClientKilometer.class);
	
	/**
	 * Creates the ClientKilometer request object and sets it up with the parameters
	 * lengthValue,fromLengthUnit,toLengthUnit
	 * After printing out the Kilometer code,
	 * @param lengthValue
	 * @param fromLengthUnit
	 * @param toLengthUnit
	 * @return
	 */
	
	public ChangeLengthUnitResponse getlenght(Double lengthValue,
											  String fromLengthUnit, String toLengthUnit){
		
		ChangeLengthUnit unit = new ChangeLengthUnit();
		unit.setLengthValue(lengthValue);
		unit.setFromLengthUnit(Lengths.fromValue(fromLengthUnit));
		unit.setToLengthUnit(Lengths.fromValue(toLengthUnit));
		
		/**
		 * Log lengh Request
		 */
		log.info("Requesting lengthValue======== " + lengthValue + "lengthValue" + fromLengthUnit + "toLengthUnit"+ toLengthUnit);
		
		
		ChangeLengthUnitResponse response =(ChangeLengthUnitResponse) getWebServiceTemplate()
				.marshalSendAndReceive("http://www.webservicex.net/length.asmx",
						unit, new SoapActionCallback("http://www.webservicex.net/ChangeLengthUnit"));
		
		return response;
	
	
	}
	
	
}
