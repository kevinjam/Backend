package controller;

import mtn.wsdl.ChangeLengthUnitResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
	public void main(String[] args){
		SpringApplication.run(Application.class);
	}
	
	
	@Bean
	CommandLineRunner lineRunner(ClientKilometer clientKilometer){
		return args -> {
			Double lengthValue=100.0;
			
			
			if (args.length > 0){
				lengthValue = 1000.0;
			}
			
			ChangeLengthUnitResponse response = clientKilometer.getlenght(100.0, "10km", "100000");
			System.err.println("==== My Response " + response.getChangeLengthUnitResult());
		};
	}
}
