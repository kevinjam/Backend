package controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class KilometerConfiguration {
	/**
	 * The marshaller is pointed at the collection of generated domain objects and will
	 * use them to both serialize and deserialize between XML and POJOs.
	 * @return
	 */
	
	@Bean
	public Jaxb2Marshaller marshaller(){
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		/**
		 * this package must match the package in the <generatePackage> specified in
		  the gradle file
		 */
		marshaller.setContextPath("mtn.wsdl");
		return marshaller;
	}
	
	/**
	 * The clientKilometer is created and configured with the URI of the Lengths service
	 * shown up above. It is also configured to use the JAXB marshaller.
	 * @param marshaller
	 * @return
	 */
	
	@Bean
	public ClientKilometer clientKilometer(Jaxb2Marshaller marshaller){
		ClientKilometer clientKilometer = new ClientKilometer();
		clientKilometer.setDefaultUri("http://www.webservicex.net/length.asmx");
		clientKilometer.setMarshaller(marshaller);
		clientKilometer.setUnmarshaller(marshaller);
		
		return clientKilometer;
	}
}
